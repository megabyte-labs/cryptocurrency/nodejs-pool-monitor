import * as firebase from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as rp from 'request-promise';
import * as puppeteer from 'puppeteer';
import * as crypto from 'crypto';
import * as validUrl from 'valid-url';
import Mailchimp = require('mailchimp-api-v3');

declare let window;
declare let XMLHttpRequest;

const app = firebase.initializeApp();
const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });
const storage = app.storage();
const fcm = firebase.messaging();
const puppeteerConfig = {
    browserWSEndpoint: functions.config().browserless.endpoint,
    headless: true
}
const pageLoadConfig = {
    waitUntil: functions.config().browserless.waituntil,
    timeout: parseInt(functions.config().browserless.timeout)
}
const errorTeaser = functions.config().messages.default_error;
const cryptoCompareApiUrl = functions.config().cryptocompare.api;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function getPools(coin: string) {
    const pools = [];
    const collection = await db.collection("pools").where("coin", "==", coin).get();
    for (const doc of collection.docs) {
        const data = doc.data();
        data.id = doc.id;
        pools.push(data);
    }
    return pools;
}

async function poolConfig(pool) {
    const options = {
        method: 'GET',
        uri: pool.api + '/config',
        json: true
    }
    const requestTime = new Date().getTime().toString();
    try {
        const response = await rp(options);
        const updates: any = {};
        if (typeof response.btc_fee === 'number' && pool.btcFee !== response.btc_fee) {
            updates.btcFee = response.btc_fee;
        }
        if (typeof response.dev_donation === 'number' && pool.devDonation !== response.dev_donation) {
            updates.devDonation = response.dev_donation;
        }
        if (typeof response.manual_payment_id === 'number' && pool.manualPaymentId !== response.manual_payment_id) { // xmrpool.net
            updates.manualPaymentId = response.manual_payment_id;
        }
        if (typeof response.manual_wallet === 'number' && pool.manualWallet !== response.manual_wallet) { // xmrpool.net
            updates.manualWallet = response.manual_wallet;
        }
        if (typeof response.divisor === 'number' && pool.divisor !== response.divisor) { // aeonminingpool.com
            updates.divisor = response.divisor;
        }
        if (typeof response.maturity_depth === 'number' && pool.maturityDepth !== response.maturity_depth) {
            updates.maturityDepth = response.maturity_depth;
        }
        if (typeof response.max_difficulty === 'number' && pool.maxDifficulty !== response.max_difficulty) { // aeonminingpool.com
            updates.maxDifficulty = response.max_difficulty;
        }
        if (typeof response.min_difficulty === 'number' && pool.minDifficulty !== response.min_difficulty) { // aeonminingpool.com
            updates.minDifficulty = response.min_difficulty;
        }
        if (typeof response.payout_feeSlew === 'number' && pool.payoutFeeSlew !== response.payout_feeSlew) { // aeonminingpool.com
            updates.payoutFeeSlew = response.payout_feeSlew;
        }
        if (typeof response.payout_timer === 'number' && pool.payoutTimer !== response.payout_timer) { // aeonminingpool.com
            updates.payoutTimer = response.payout_timer;
        }
        if (typeof response.min_btc_payout === 'number' && pool.minBtcPayout !== response.min_btc_payout) {
            updates.minBtcPayout = response.min_btc_payout;
        }
        if (typeof response.min_denom === 'number' && pool.minDenom !== response.min_denom) {
            updates.minDenom = response.min_denom;
        }
        if (typeof response.min_exchange_payout === 'number' && pool.minExchangePayout !== response.min_exchange_payout) {
            updates.minExchangePayout = response.min_exchange_payout;
        }
        if (typeof response.min_wallet_payout === 'number' && pool.minWalletPayout !== response.min_wallet_payout) {
            updates.minWalletPayout = response.min_wallet_payout;
        }
        if (typeof response.pool_dev_donation === 'number' && pool.poolDevDonation !== response.pool_dev_donation) {
            updates.poolDevDonation = response.pool_dev_donation;
        }
        if (typeof response.pplns_fee === 'number' && pool.pplnsFee !== response.pplns_fee) {
            updates.pplnsFee = response.pplns_fee;
        }
        if (typeof response.pps_fee === 'number' && pool.ppsFee !== response.ppsFee) {
            updates.ppsFee = response.pps_fee;
        }
        if (typeof response.solo_fee === 'number' && pool.soloFee !== response.solo_fee) {
            updates.soloFee = response.solo_fee;
        }
        if (Object.keys(updates).length) {
            updates.lastConfigUpdate = new Date().getTime();
            await db.collection("pools").doc(pool.id).update(updates);
            await db.collection("pools").doc(pool.id).collection("configHistory").doc(requestTime).set(updates);
        }
    } catch (e) {
        console.error(pool.api + '/config poolConfig() error', e);
        let failureCount = 1;
        if (pool.failureCount) {
            failureCount = pool.failureCount + 1;
        }
        await db.collection("pools").doc(pool.id).update({
            failureCount: failureCount,
            lastFailure: parseInt(requestTime)
        });
        await db.collection("pools").doc(pool.id).collection("failures").doc(requestTime).set({
            endpoint: "/config",
            requestTime: parseInt(requestTime)
        });
    }
}

async function poolStats(pool, endpoint: string, saveHistory: boolean, hourTick: boolean, dayTick: boolean) {
    const options = {
        method: 'GET',
        uri: pool.api + endpoint,
        json: true
    }
    const requestTime = new Date().getTime().toString();
    let prettyName = 'ShouldNotExist';
    if (endpoint === '/pool/stats') {
        prettyName = 'Pool';
    } else if (endpoint === '/pool/stats/solo') {
        prettyName = 'Solo';
    } else if (endpoint === '/pool/stats/pplns') {
        prettyName = 'PPLNS';
    } else if (endpoint === '/pool/stats/pps') {
        prettyName = 'PPS';
    }
    try {
        const response = await rp(options);
        const updates: any = {};
        const historyUpdates: any = {};
        const moneroOceanUpdates: any = {};
        if (endpoint === '/pool/stats') {
            if (typeof response.last_payment === 'number' && pool.lastPayment !== response.last_payment) {
                updates.lastPayment = response.last_payment;
            }
            if (response.pool_list && response.pool_list.length > 0) {
                if (response.pool_list.indexOf('solo') !== -1) {
                    if (pool.supportsSolo !== true) {
                        updates.supportsSolo = true;
                    }
                } else {
                    if (pool.supportsSolo !== false) {
                        updates.supportsSolo = false;
                    }
                }
                if (response.pool_list.indexOf('pps') !== -1) {
                    if (pool.supportsPPS !== true) {
                        updates.supportsPPS = true;
                    }
                } else {
                    if (pool.supportsPPS !== false) {
                        updates.supportsPPS = false;
                    }
                }
                if (response.pool_list.indexOf('pplns') !== -1) {
                    if (pool.supportsPPLNS !== true) {
                        updates.supportsPPLNS = true;
                    }
                } else {
                    if (pool.supportsPPLNS !== false) {
                        updates.supportsPPLNS = false;
                    }
                }
            }
        }
        if (typeof response.pool_statistics === 'object' && Object.keys(response.pool_statistics).length) {
            const stats = response.pool_statistics;
            if (typeof stats.hashRate === 'number') {
                historyUpdates.hashRate = stats.hashRate;
                if (stats.hashRate !== pool['hashRate' + prettyName]) {
                    updates['hashRate' + prettyName] = stats.hashRate;
                }
            }
            if (typeof stats.lastBlockFound === 'number') {
                historyUpdates.lastBlockFound = stats.lastBlockFound;
                if (stats.lastBlockFound !== pool['lastBlockFound' + prettyName]) {
                    updates['lastBlockFound' + prettyName] = stats.lastBlockFound;
                }
            }
            if (typeof stats.lastBlockFoundTime === 'number') {
                historyUpdates.lastBlockFoundTime = stats.lastBlockFoundTime;
                if (stats.lastBlockFoundTime !== pool['lastBlockFoundTime' + prettyName]) {
                    updates['lastBlockFoundTime' + prettyName] = stats.lastBlockFoundTime;
                }
            }
            if (typeof stats.miners === 'number') {
                historyUpdates.miners = stats.miners;
                if (stats.miners !== pool['miners' + prettyName]) {
                    updates['miners' + prettyName] = stats.miners;
                }
            }
            if (typeof stats.roundHashes === 'number') {
                historyUpdates.roundHashes = stats.roundHashes;
                if (stats.roundHashes !== pool['roundHashes' + prettyName]) {
                    updates['roundHashes' + prettyName] = stats.roundHashes;
                }
            }
            if (typeof stats.totalBlocksFound === 'number') {
                historyUpdates.totalBlocksFound = stats.totalBlocksFound;
                if (stats.totalBlocksFound !== pool['totalBlocksFound' + prettyName]) {
                    updates['totalBlocksFound' + prettyName] = stats.totalBlocksFound;
                }
            }
            if (typeof stats.totalHashes === 'number') {
                historyUpdates.totalHashes = stats.totalHashes;
                if (stats.totalHashes !== pool['totalHashes' + prettyName]) {
                    updates['totalHashes' + prettyName] = stats.totalHashes;
                }
            }
            if (typeof stats.totalMinersPaid === 'number') {
                historyUpdates.totalMinersPaid = stats.totalMinersPaid;
                if (stats.totalMinersPaid !== pool['totalMinersPaid' + prettyName]) {
                    updates['totalMinersPaid' + prettyName] = stats.totalMinersPaid;
                }
            }
            if (typeof stats.totalPayments === 'number') {
                historyUpdates.totalPayments = stats.totalPayments;
                if (stats.totalPayments !== pool['totalPayments' + prettyName]) {
                    updates['totalPayments' + prettyName] = stats.totalPayments;
                }
            }
            if (pool.software === 'monero-ocean') {
                if (typeof stats.activePort === 'number') {
                    moneroOceanUpdates.activePort = stats.activePort;
                }
                if (typeof stats.activePortProfit === 'number') {
                    moneroOceanUpdates.activePortProfit = stats.activePortProfit;
                }
                if (typeof stats.activePortComment === 'string') {
                    moneroOceanUpdates.activePortComment = stats.activePortComment;
                }
                if (typeof stats.activePorts === 'object' && stats.activePorts.length) {
                    moneroOceanUpdates.activePorts = stats.activePorts;
                }
                if (typeof stats.altBlocksFound === 'object' && Object.keys(stats.altBlocksFound).length) {
                    moneroOceanUpdates.altBlocksFound = stats.altBlocksFound;
                }
                if (typeof stats.coinComment === 'object' && Object.keys(stats.coinComment).length) {
                    moneroOceanUpdates.coinComment = stats.coinComment;
                }
                if (typeof stats.coinProfit === 'object' && Object.keys(stats.coinProfit).length) {
                    moneroOceanUpdates.coinProfit = stats.coinProfit;
                }
                if (typeof stats.currentEfforts === 'object' && Object.keys(stats.currentEfforts).length) {
                    moneroOceanUpdates.currentEfforts = stats.currentEfforts;
                }
                if (typeof stats.minBlockRewards === 'object' && Object.keys(stats.minBlockRewards).length) {
                    moneroOceanUpdates.minBlockRewards = stats.minBlockRewards;
                }
                if (typeof stats.pending === 'number') {
                    moneroOceanUpdates.pending = stats.pending;
                }
                if (typeof stats.portHash === 'object' && Object.keys(stats.portHash).length) {
                    moneroOceanUpdates.portHash = stats.portHash;
                }
                if (typeof stats.portMinerCount === 'object' && Object.keys(stats.portMinerCount).length) {
                    moneroOceanUpdates.portMinerCount = stats.portMinerCount;
                }
                if (typeof stats.pplnsPortShares === 'object' && Object.keys(stats.pplnsPortShares).length) {
                    moneroOceanUpdates.pplnsPortShares = stats.pplnsPortShares;
                }
                if (typeof stats.pplnsWindowTime === 'number') {
                    moneroOceanUpdates.pplnsWindowTime = stats.pplnsWindowTime;
                }
                if (typeof stats.totalAltBlocksFound === 'number') {
                    moneroOceanUpdates.totalAltBlocksFound = stats.totalAltBlocksFound;
                }
                if (Object.keys(moneroOceanUpdates).length) {
                    try {
                        await db.collection("pools").doc(pool.id).collection("moneroOcean").doc(prettyName).update(moneroOceanUpdates);
                    } catch (e) {
                        console.error('Error updating moneroOcean doc, probably does not exist - creating', e);
                        await db.collection("pools").doc(pool.id).collection("moneroOcean").doc(prettyName).set(moneroOceanUpdates);
                    }
                }
            }
        }
        if (Object.keys(updates).length) {
            await db.collection("pools").doc(pool.id).update(updates);
            if (saveHistory) {
                if (hourTick) {
                    historyUpdates.hourTick = true;
                }
                if (dayTick) {
                    historyUpdates.dayTick = true;
                }
                await db.collection("pools").doc(pool.id).collection("statsHistory" + prettyName).doc(requestTime).set(historyUpdates);
            }
        }
    } catch (e) {
        console.error(pool.api + endpoint + ' poolStats() error', e);
        let failureCount = 1;
        if (pool.failureCount) {
            failureCount = pool.failureCount + 1;
        }
        await db.collection("pools").doc(pool.id).update({
            failureCount: failureCount,
            lastFailure: parseInt(requestTime)
        });
        await db.collection("pools").doc(pool.id).collection("failures").doc(requestTime).set({
            requestTime: parseInt(requestTime),
            endpoint: endpoint
        });
    }
}

async function updateBlock(pool, block) {
    try {
        if (block.diff && typeof block.hash === 'string' && block.height && typeof block.pool_type === 'string' && block.shares && block.ts && block.hasOwnProperty('unlocked') && block.hasOwnProperty('valid') && block.value) {
            if (block.diff === 'string') {
                block.diff = parseInt(block.diff);
            }
            if (block.height === 'string') {
                block.height = parseInt(block.height);
            }
            if (block.shares === 'string') {
                block.shares = parseInt(block.shares);
            }
            if (block.ts === 'string') {
                block.ts = parseInt(block.ts);
            }
            if (block.value === 'string') {
                block.value = parseInt(block.value);
            }
            if (block.unlocked === 'string' && (block.unlocked === 'true' || block.unlocked === 'false')) {
                block.unlocked = block.unlocked === 'true';
            }
            if (block.valid === 'string' && (block.valid === 'true' || block.valid === 'false')) {
                block.valid = block.valid === 'true';
            }
            if (block.shares && block.diff) {
                block.luck = block.shares / block.diff * 100;
            }
            const blockData = await db.collection("pools").doc(pool.id).collection("blocks").doc(block.hash).get();
            if (blockData.exists) {
                await db.collection("pools").doc(pool.id).collection("blocks").doc(block.hash).update(block);
                block.coin = pool.coin;
                block.poolId = pool.id;
                block.poolName = pool.name;
                block.coinPoolType = pool.coin + '-' + block.pool_type;
                const mainBlockData = await db.collection("blocks").doc(pool.uuid + '___' + block.hash).get();
                if (mainBlockData.exists) {
                    await db.collection("blocks").doc(pool.uuid + '___' + block.hash).update(block);
                } else {
                    await db.collection("blocks").doc(pool.uuid + '___' + block.hash).set(block);
                }
            } else {
                await db.collection("pools").doc(pool.id).collection("blocks").doc(block.hash).set(block);
                block.coin = pool.coin;
                block.poolId = pool.id;
                block.poolName = pool.name;
                block.coinPoolType = pool.coin + '-' + block.pool_type;
                const mainBlockData = await db.collection("blocks").doc(pool.uuid + '___' + block.hash).get();
                if (mainBlockData.exists) {
                    await db.collection("blocks").doc(pool.uuid + '___' + block.hash).update(block);
                } else {
                    await db.collection("blocks").doc(pool.uuid + '___' + block.hash).set(block);
                }
            }
        } else {
            console.error('Error with updateBlock() - does not contain necessary information. Block info:', block);
        }
    } catch (e) {
        console.error('Error with updateBlock()', e);
    }
}

async function poolBlocks(pool) {
    const options = {
        method: 'GET',
        uri: pool.api + '/pool/blocks?page=0&limit=100',
        json: true
    }
    const requestTime = new Date().getTime().toString();
    try {
        const response = await rp(options);
        const promises = [];
        let lastUnlockedBlock;
        for (const block of response) {
            if (block.hash === pool.lastUnlockedBlock) break;
            if (block.unlocked) lastUnlockedBlock = block.hash;
            promises.push(updateBlock(pool, block));
        }
        try {
            await Promise.all(promises);
            if (lastUnlockedBlock) {
                await db.collection("pools").doc(pool.id).update({
                    lastUnlockedBlock: lastUnlockedBlock
                });
            }
        } catch (e) {
            console.error('poolBlocks() Promise.all error', e);
        }
    } catch (e) {
        console.error(pool.api + '/pool/blocks?page=0&limit=100 poolBlocks() error', e);
        let failureCount = 1;
        if (pool.failureCount) {
            failureCount = pool.failureCount + 1;
        }
        await db.collection("pools").doc(pool.id).update({
            failureCount: failureCount,
            lastFailure: parseInt(requestTime)
        });
        await db.collection("pools").doc(pool.id).collection("failures").doc(requestTime).set({
            requestTime: parseInt(requestTime),
            endpoint: '/pool/blocks?page=0&limit=100 error'
        });
    }
}

async function updatePayment(pool, payment) {
    try {
        if (payment.fee && typeof payment.hash === 'string' && payment.id && typeof payment.hasOwnProperty('mixins') && typeof payment.hasOwnProperty('payees') && typeof payment.pool_type === 'string' && payment.ts && payment.value) {
            if (typeof payment.fee === 'string') {
                payment.fee = parseInt(payment.fee);
            }
            if (typeof payment.id === 'string') {
                payment.id = parseInt(payment.fee);
            }
            if (typeof payment.mixins === 'string') {
                payment.mixins = parseInt(payment.mixins);
            }
            if (typeof payment.ts === 'string') {
                payment.ts = parseInt(payment.ts);
            }
            if (typeof payment.value === 'string') {
                payment.value = parseInt(payment.value);
            }
            const paymentData = await db.collection("pools").doc(pool.id).collection("payments").doc(payment.hash).get();
            if (paymentData.exists) {
                await db.collection("pools").doc(pool.id).collection("payments").doc(payment.hash).update(payment);
                payment.coin = pool.coin;
                payment.poolId = pool.id;
                payment.poolName = pool.name;
                payment.coinPoolType = pool.coin + '-' + payment.pool_type;
                const mainPaymentData = await db.collection("payments").doc(pool.uuid + '___' + payment.hash).get();
                if (mainPaymentData.exists) {
                    await db.collection("payments").doc(pool.uuid + '___' + payment.hash).update(payment);
                } else {
                    await db.collection("payments").doc(pool.uuid + '___' + payment.hash).set(payment);
                }
            } else {
                await db.collection("pools").doc(pool.id).collection("payments").doc(payment.hash).set(payment);
                payment.coin = pool.coin;
                payment.poolId = pool.id;
                payment.poolName = pool.name;
                payment.coinPoolType = pool.coin + '-' + payment.pool_type;
                const mainPaymentData = await db.collection("payments").doc(pool.uuid + '___' + payment.hash).get();
                if (mainPaymentData.exists) {
                    await db.collection("payments").doc(pool.uuid + '___' + payment.hash).update(payment);
                } else {
                    await db.collection("payments").doc(pool.uuid + '___' + payment.hash).set(payment);
                }
            }
        } else {
            console.error('Error with updatePayment() - does not contain necessary information. Payment info:', payment);
        }
    } catch (e) {
        console.error('Error with updatePayment()', e);
    }
}

async function poolPayments(pool) {
    const options = {
        method: 'GET',
        uri: pool.api + '/pool/payments?page=0&limit=500',
        json: true
    }
    const requestTime = new Date().getTime().toString();
    try {
        const response = await rp(options);
        const promises = [];
        for (const payment of response) {
            if (payment.hash === pool.lastPaymentHash) break;
            promises.push(updatePayment(pool, payment));
        }
        try {
            await Promise.all(promises);
            if (response[0] && response[0].hash) {
                await db.collection("pools").doc(pool.id).update({
                    lastPaymentHash: response[0].hash
                });
            }
        } catch (e) {
            console.error('poolPayments() Promise.all error', e);
        }
    } catch (e) {
        console.error(pool.api + '/pool/payments?page=0&limit=500 poolPayments() error', e);
        let failureCount = 1;
        if (pool.failureCount) {
            failureCount = pool.failureCount + 1;
        }
        await db.collection("pools").doc(pool.id).update({
            failureCount: failureCount,
            lastFailure: parseInt(requestTime)
        });
        await db.collection("pools").doc(pool.id).collection("failures").doc(requestTime).set({
            endpoint: '/pool/payments?page=0&limit=500 error',
            requestTime: parseInt(requestTime)
        });
    }
}

async function poolPorts(pool) {
    const options = {
        method: 'GET',
        uri: pool.api + '/pool/ports',
        json: true
    }
    const requestTime = new Date().getTime().toString();
    try {
        const response = await rp(options);
        try {
            await db.collection("pools").doc(pool.id).collection("ports").doc("config").update(response);
        } catch (e) {
            console.error('Error setting pool port config - most likely need to create document', e);
            await db.collection("pools").doc(pool.id).collection("ports").doc("config").set(response);
        }
    } catch (e) {
        console.error(pool.api + '/pool/ports poolPorts() error', e);
        let failureCount = 1;
        if (pool.failureCount) {
            failureCount = pool.failureCount + 1;
        }
        await db.collection("pools").doc(pool.id).update({
            failureCount: failureCount,
            lastFailure: parseInt(requestTime)
        });
        await db.collection("pools").doc(pool.id).collection("failures").doc(requestTime).set({
            endpoint: '/pool/ports',
            requestTime: parseInt(requestTime)
        });
    }
}

async function getCounter(id: string) {
    const doc = await db.collection("counters").doc(id).get();
    if (doc.exists) {
        const data = doc.data();
        if (typeof data.counter === 'number') {
            return data.counter;
        } else {
            console.error('No counter variables exists with getCounter() for ' + id);
            return 1;
        }
    } else {
        await db.collection("counters").doc(id).set({ counter: 1 });
        return 1;
    }
}

async function runUpdate(coin: string) {
    const pools = await getPools(coin);
    const promises = [];
    for (const pool of pools) {
        const count = await getCounter(pool.id);
        const fiveMinuteTick = count % 2 === 0; // every 2 minutes
        const hourTick = count % 60 === 0; // 1 hour
        const dailyTick = count % 1440 === 0; // 1 day
        promises.push(db.collection("counters").doc(pool.id).update({ counter: count + 1 }));
        if (pool.software === 'nodejs-pool' || pool.software === 'monero-ocean') {
            promises.push(poolStats(pool, '/pool/stats', fiveMinuteTick, hourTick, dailyTick));
            promises.push(poolStats(pool, '/pool/stats/pplns', fiveMinuteTick, hourTick, dailyTick));
            promises.push(poolStats(pool, '/pool/stats/solo', fiveMinuteTick, hourTick, dailyTick));
            promises.push(poolStats(pool, '/pool/stats/pps', fiveMinuteTick, hourTick, dailyTick));
            promises.push(poolConfig(pool));
            promises.push(poolBlocks(pool));
            promises.push(poolPayments(pool));
            promises.push(poolPorts(pool));
        }
    }
    await Promise.all(promises).catch(e => {
        console.error('XMRUpdate Promise.all error', e);
    });
}

export const XMRUpdate = functions.pubsub.topic('minute-tick').onPublish(async () => {
    const coin = 'XMR';
    await runUpdate(coin);
});

export const LOKIUpdate = functions.pubsub.topic('minute-tick').onPublish(async () => {
    await runUpdate('LOKI');
});

export const AEONUpdate = functions.pubsub.topic('minute-tick').onPublish(async () => {
    await runUpdate('AEON');
});

async function saveFullPriceData(coin) {
    const options = {
        method: 'GET',
        uri: cryptoCompareApiUrl + '/pricemultifull?fsyms=' + coin.id + '&tsyms=USD',
        json: true
    }
    const updates: any = {};
    const response = await rp(options);
    if (typeof response['RAW'] === 'object' && Object.keys(response['RAW']).length && typeof response['RAW'][coin.id] === 'object' && Object.keys(response['RAW'][coin.id]).length && typeof response['RAW'][coin.id]['USD'] === 'object' && Object.keys(response['RAW'][coin.id]['USD']).length) {
        const prices = response['RAW'][coin.id]['USD'];
        const currentDoc = await db.collection("coins").doc(coin.id).collection("fullPriceData").doc("cryptocompare").get();
        if (currentDoc.exists) {
            await db.collection("coins").doc(coin.id).collection("fullPriceData").doc("cryptocompare").update(prices);
        } else {
            await db.collection("coins").doc(coin.id).collection("fullPriceData").doc("cryptocompare").set(prices);
        }
        if (typeof prices['PRICE'] === 'number' && prices['PRICE'] !== coin.usdPrice) {
            updates.usdPrice = prices['PRICE'];
        }
        if (typeof prices['CHANGEPCT24HOUR'] === 'number' && prices['CHANGEPCT24HOUR'] !== coin.changePercent24Hour) {
            updates.changePercent24Hour = prices['CHANGEPCT24HOUR'];
        }
        if (Object.keys(updates).length) {
            await db.collection("coins").doc(coin.id).update(updates);
        }
    } else {
        console.error('Invalid response from cryptocompare in saveFullPriceData()', response);
    }
}

async function saveHistoricalDataPoint(coin, type: string, dataPoint) {
    if (typeof dataPoint.time === 'number') {
        const dataPointDoc = await db.collection("coins").doc(coin.id).collection(type + "History").doc(dataPoint.time).get();
        if (dataPointDoc.exists) {
            await db.collection("coins").doc(coin.id).collection(type + "History").doc(dataPoint.time).update(dataPoint);
        } else {
            await db.collection("coins").doc(coin.id).collection(type + "History").doc(dataPoint.time).set(dataPoint);
        }
    } else {
        console.error('dataPoint missing time in saveHistoricalDataPoint()');
    }
}

async function saveHistoricalData(coin, type: string) {
    let limit = 10;
    if (type === 'minute') {
        limit = 10;
    } else if (type === 'hour') {
        limit = 3;
    } else if (type === 'day') {
        limit = 3;
    }
    const options = {
        method: 'GET',
        uri: cryptoCompareApiUrl + '/histo' + type + '?fsym' + coin.id + '&limit=' + limit,
        json: true
    }
    const promises = [];
    const response = await rp(options);
    if (typeof response['Data'] === 'object' && response['Data'].length) {
        for (const dataPoint of response['Data']) {
            promises.push(saveHistoricalDataPoint(coin, type, dataPoint));
        }
        await Promise.all(promises).catch(e => {
            console.error('Failed to save historical data points', e);
        });
    }
}

async function updateNetworkStats(coin) {
    try {
        const options = {
            method: 'GET',
            uri: coin.api + '/network/stats',
            json: true
        }
        const response = await rp(options);
        if (response) {
            const updates: any = {};
            if (response.difficulty) {
                updates.networkDifficulty = parseInt(response.difficulty);
            }
            if (response.hash) {
                updates.networkHash = updates.hash;
            }
            if (response.height) {
                updates.networkHeight = parseInt(updates.height);
            }
            if (response.value) {
                updates.networkValue = parseInt(updates.value);
            }
            if (response.ts) {
                updates.networkTs = parseInt(updates.ts);
            }
            await db.collection("coins").doc(coin.id).update(updates);
        } else {
            console.error('Failed to get response in updateNetworkStats()');
        }
    } catch (e) {
        console.error('Failed to updateNetworkStats() for ' + coin.id);
    }
}

export const NetworkAndPricesUpdate = functions.pubsub.topic('minute-tick').onPublish(async () => {
    const coins = [];
    const promises = [];
    const collection = await db.collection("coins").get();
    for (const doc of collection.docs) {
        const data = doc.data();
        data.id = doc.id;
        coins.push(data);
    }
    for (const coin of coins) {
        promises.push(updateNetworkStats(coin));
        promises.push(saveFullPriceData(coin));
        promises.push(saveHistoricalData(coin, 'minute'));
    }
    await Promise.all(promises).catch(e => {
        console.error('Promise.all error with CoinPricesMinute', e);
    });
});

export const CoinPricesHour = functions.pubsub.topic('hourly-tick').onPublish(async () => {
    const coins = [];
    const promises = [];
    const collection = await db.collection("coins").get();
    for (const doc of collection.docs) {
        const data = doc.data();
        data.id = doc.id;
        coins.push(data);
        promises.push(data.id);
    }
    for (const coin of coins) {
        promises.push(saveHistoricalData(coin, 'hour'));
    }
    await Promise.all(promises).catch(e => {
        console.error('Promise.all error with CoinPricesHourly', e);
    });
});

export const CoinPricesDaily = functions.pubsub.topic('daily-tick').onPublish(async () => {
    const coins = [];
    const promises = [];
    const collection = await db.collection("coins").get();
    for (const doc of collection.docs) {
        const data = doc.data();
        data.id = doc.id;
        coins.push(data);
        promises.push(data.id);
    }
    for (const coin of coins) {
        promises.push(saveHistoricalData(coin, 'day'));
    }
    await Promise.all(promises).catch(e => {
        console.error('Promise.all error with CoinPricesHourly', e);
    });
});

export const AddNewPool = functions.https.onRequest(async (request, response) => {
    let id, url, api, nextUid, software;
    if (!request.body.url || !request.body.coin || !request.body.name) {
        response.status(418).json({ message: errorTeaser });
    } else {
        if (!validUrl.isHttpsUri(request.body.url)) {
            response.status(500).json({ message: "The URL you submitted is not well-formed HTTPS URL." });
        } else {
            const coinDoc = await db.collection("coins").doc(request.body.coin).get();
            let coinData;
            if (coinDoc.exists) {
                coinData = coinDoc.data();
                if (coinData.nextUid && typeof coinData.nextUid === 'number') {
                    nextUid = coinData.nextUid;
                    await db.collection("coins").doc(request.body.coin).update({
                        nextUid: nextUid + 1
                    });
                } else {
                    nextUid = 1;
                    await db.collection("coins").doc(request.body.coin).update({
                        nextUid: 2
                    });
                }
            }
            if (coinDoc.exists && coinData.supported) {
                id = request.body.url.replace('https://', '');
                id = id.substring(0, id.indexOf('/'));
                url = 'https://' + id;
                const browser = await puppeteer.connect(puppeteerConfig);
                const page = await browser.newPage();
                await page.goto(request.body.url, pageLoadConfig);
                await page.evaluate(function () {
                    function addXMLRequestCallback(callback) {
                        let oldSend, i;
                        if (XMLHttpRequest.callbacks) {
                            // we've already overridden send() so just add the callback
                            XMLHttpRequest.callbacks.push(callback);
                        } else {
                            // create a callback queue
                            XMLHttpRequest.callbacks = [callback];
                            // store the native send()
                            oldSend = XMLHttpRequest.prototype.send;
                            // override the native send()
                            XMLHttpRequest.prototype.send = function () {
                                // process the callback queue
                                // the xhr instance is passed into each callback but seems pretty useless
                                // you can't tell what its destination is or call abort() without an error
                                // so only really good for logging that a request has happened
                                // I could be wrong, I hope so...
                                // EDIT: I suppose you could override the onreadystatechange handler though
                                for (i = 0; i < XMLHttpRequest.callbacks.length; i++) {
                                    XMLHttpRequest.callbacks[i](this);
                                }
                                // call the native send()
                                oldSend.apply(this, arguments);
                            }
                        }
                    }
                    addXMLRequestCallback(function (xhr) {
                        const responseUrl = xhr.responseURL;
                        if (responseUrl.substring(responseUrl.length - 11) === '/pool/stats') {
                            window.megabyteAPIUrl = url.replace('/pool/stats', '');
                        } else if (responseUrl.substring(responseUrl.length - 14) === '/network/stats') {
                            window.megabyteAPIUrl = url.replace('/network/stats', '');
                        }
                    });
                });
                await sleep(20 * 1000);
                api = await page.evaluate(() => {
                    return window.megabyteAPIUrl;
                });
                if (api) {
                    const configOptions = {
                        method: 'GET',
                        uri: api + '/config',
                        json: true
                    }
                    try {
                        const configResponse = await rp(configOptions);
                        if (typeof configResponse.dev_donation === 'number' && typeof configResponse.pool_dev_donation === 'number' && typeof configResponse.pplns_fee === 'number' && typeof configResponse.pps_fee === 'number' && typeof configResponse.solo_fee === 'number') {
                            const statsOptions = {
                                method: 'GET',
                                uri: api + '/pool/stats',
                                json: true
                            }
                            const statsResponse = await rp(statsOptions);
                            if (typeof statsResponse.pool_statistics === 'object' && typeof statsResponse.pool_statistics.miners === 'number') {
                                if (statsResponse.pool_statistics.miners > 4) {
                                    if (typeof statsResponse.pool_statistics.activePortProfit === 'number') {
                                        software = 'monero-ocean';
                                    } else {
                                        software = 'nodejs-pool';
                                    }
                                    const poolDoc = await db.collection("pools").doc(id).get();
                                    if (poolDoc.exists) {
                                        console.error('Pool already exists', id);
                                        response.status(500).json({ message: "Failed to add pool to database. Another pool already exists with similar parameters." });
                                    } else {
                                        await db.collection("pools").doc(id).set({
                                            coin: request.body.coin,
                                            api: api,
                                            url: url,
                                            name: request.body.name,
                                            uuid: request.body.coin + nextUid,
                                            software: software
                                        });
                                        response.status(200).json({ message: "Successfully added the pool to the catalog." });
                                    }
                                } else {
                                    response.status(500).json({ message: "There are only " + statsResponse.pool_statistics.miners + " connected to this pool. To add a pool, there must be at least 5 active miners." });
                                }
                            } else {
                                response.status(500).json({ message: "Unable to detect the number of miners currently using the pool." })
                            }
                        } else {
                            console.error("This site's API is missing some critical information.")
                            response.status(500).json({ message: "This site's API is missing some critical information.", configResponse });
                        }
                    } catch (e) {
                        console.error('Failed to connect to the configuration endpoint.' + request.body.url, e);
                        response.status(500).json({ message: "Failed to connect to the configuration endpoint." })
                    }
                } else {
                    console.error('Failed to automatically identify the API URL. This pool type is not supported yet.', request.body.url);
                    response.status(500).json({ message: "Failed to automatically identify the API URL. This pool type is not supported yet." })
                }
            } else {
                console.error('This coin is not currently supported: ' + request.body.coin);
                let message = errorTeaser;
                if (coinData.disabledReason) {
                    message = coinData.disabledReason;
                }
                response.status(500).json({ message: message });
            }
        }
    }
});

function getMailChimpHash(email) {
    return crypto.createHash('md5').update(email.toLowerCase()).digest("hex");
}

export const NewsletterSignUp = functions.https.onRequest((request, response) => {
    if (request.method === 'POST') {
        if (request.body.email) {
            const mailchimp = new Mailchimp(functions.config().mailchimp.api_key);
            const body: any = {
                email_address: request.body.email,
                status: "subscribed"
            };
            mailchimp
                .post({
                    path: "/lists/" + functions.config().mailchimp.list_id + "/members",
                    body: body
                })
                .then(() => {
                    response.status(200).json({ message: "Successfully added to list" });
                })
                .catch((err) => {
                    if (err.title === 'Member Exists') {
                        response.status(500).json({ message: "User is already on list", error: err.title });
                    } else {
                        console.error(
                            request.body.email +
                            " failed to be added to the MailChimp list",
                            err
                        );
                        response.status(500).json({ message: "Failed to be added to list", error: err.title });
                    }
                });
        } else {
            response.status(418).json({ message: errorTeaser });
        }
    } else {
        response.status(418).json({ message: errorTeaser });
    }
});