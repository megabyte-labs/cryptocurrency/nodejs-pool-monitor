importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/3.2.0/workbox-sw.js",
  "./assets/js/firebase-app.js",
  "./assets/js/firebase-messaging.js"
);

if (workbox) {
  workbox.setConfig({
    debug: false
  });
  workbox.core.setCacheNameDetails({
    prefix: "megabyte-space",
    suffix: "v1"
  });
  workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);
  workbox.skipWaiting();
  workbox.clientsClaim();
  workbox.precaching.precacheAndRoute([]);
  /*workbox.precaching.precacheAndRoute([{
    "url": "/assets/fonts/ionicons.woff2?v=3.0.0-alpha.3"
  }]);*/
  workbox.routing.registerNavigationRoute("/index.html", {
    whitelist: [
      new RegExp(/^\/$/),
      new RegExp(/^\/home/),
      new RegExp(/^\/search/),
      new RegExp(/^\/coming-soon/),
      new RegExp(/^\/settings/),
      new RegExp(/^\/privacy-policy/),
      new RegExp(/^\/terms-of-service/),
      new RegExp(/^\/unsubscribe/)
    ],
    blacklist: [
      new RegExp(/.xml$/),
      new RegExp(/opensearch/),
      new RegExp(/api/)
    ]
  });
  workbox.routing.registerRoute(
    /\.(?:png|svg|jpg|webp|ico|woff|woff2|eot|ttf)$/,
    workbox.strategies.cacheFirst({
      cacheName: "misc-static-cache"
    })
  );
  workbox.routing.registerRoute(
    /\/assets\/.*/,
    workbox.strategies.cacheFirst({
      cacheName: "asset-cache"
    })
  );
  workbox.googleAnalytics.initialize();
}

var messaging;
if ((self.Notification || self.webkitNotifications)) {
  firebase.initializeApp({
    messagingSenderId: "43634632651"
  });

  messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(function (payload) {
    if (payload.notification && payload.notification.type) {
      var title = payload.notification.title ? payload.notification.title : "Find the Best New Music | Music Blobs";
      var body = payload.notification.body ? payload.notification.body : "Check out the latest features of Music Blobs and discover awesome new music.";
      var image = payload.notification.image ? payload.notification.image : 'https://megabyte.space/assets/icon/apple-touch-icon.png';
      var click_action = payload.notification.click_action ? payload.notification.click_action : "https://megabyte.space/home";
      var notificationTitle = title;
      var notificationOptions = {
        body: body,
        icon: image,
        click_action: click_action
      };

      return self.registration.showNotification(notificationTitle,
        notificationOptions);
    } else {
      return null;
    }
  });
}
