export const environment = {
  production: true,
  butter: {
    api_key: '9aa0764eabc5be94e8594f865117e2b6769e11ac'
  },
  coins: [
    {
      name: 'Monero',
      abbrev: 'XMR'
    },
    {
      name: 'AEON',
      abbrev: 'AEON'
    },
    {
      name: 'Electroneum',
      abbrev: 'ETN'
    },
    {
      name: 'Sumokoin',
      abbrev: 'SUMO'
    }
  ],
  chartColors: [
    'rgba(25,118,210,0.59)',
    'rgba(211,47,47,0.59)',
    'rgba(0,121,107,0.59)',
    'rgba(158,158,158,0.59)',
    'rgba(69,90,100,0.59)',
    'rgba(49,27,146,0.59)',
    'rgba(144,202,249,0.59)',
    'rgba(41,98,255,0.59)'
  ],
  poolCharts: {
    'miners': {
      label: 'Miners',
      ylabel: 'Number of Miners Active'
    },
    'hashRate': {
      label: 'Hash Rate',
      ylabel: 'Pool Hash Rate'
    },
    'totalBlocksFound': {
      label: 'Blocks Found',
      ylabel: 'Total Number of Blocks Found'
    },
    'totalHashes': {
      label: 'Total Hashes',
      ylabel: 'Total Number of Recorded Hashes'
    },
    'totalMinersPaid': {
      label: 'Miners Paid',
      ylabel: 'Total Number of Miners Paid (Unique Wallets)'
    },
    'totalPayments': {
      label: 'Total Payments',
      ylabel: 'Total Number of Payments Made by Pool'
    },
    'roundHashes': {
      label: 'Round Hashes',
      ylabel: 'Number of Hashes Used for Current Block Round'
    }
  },
  enabledCoins: ['XMR', 'AEON', 'ETN', 'SUMO'],
  endpoint: {
    newsletter: 'https://megabyte.space/api/email/subscribe',
    submitPool: 'https://megabyte.space/api/pool/submit'
  },
  firebase: {
    apiKey: 'AIzaSyAki8ZNZeepND3jf4jUiANDEDzaQ5wtf74',
    authDomain: 'megabyte-space.firebaseapp.com',
    databaseURL: 'https://megabyte-space.firebaseio.com',
    projectId: 'megabyte-space',
    storageBucket: 'megabyte-space.appspot.com',
    messagingSenderId: '43634632651'
  }
};
