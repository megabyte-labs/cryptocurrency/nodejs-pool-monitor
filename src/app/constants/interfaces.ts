export interface ExchangeSocketMessage {
  FLAGS?: string,
  FROMSYMBOL?: string,
  HIGH24HOUR?: number,
  HIGHHOUR?: number,
  LASTMARKET?: string,
  LASTTRADEID?: number,
  LASTUPDATE?: number,
  LASTVOLUME?: number,
  LASTVOLUMETO?: number,
  LOW24HOUR?: number,
  LOWHOUR?: number,
  MARKET?: string,
  OPEN24HOUR?: number,
  OPENHOUR?: number,
  PRICE?: number,
  TOSYMBOL?: string,
  TYPE?: string,
  VOLUME24HOUR?: number,
  VOLUME24HOURTO?: number,
  VOLUMEHOUR?: number,
  VOLUMEHOURTO?: number;
}

export interface Coin {
  changePercent24Hour?: number;
  supported?: boolean;
  usdPrice?: number;
  name: string;
}

export interface CoinWithID extends Coin {
  id: string;
}

export interface Pool {
  api: string;
  coin: string;
  url: string;
  uuid: string;
  name: string;
  software: string;
  status?: string;
  hashRatePool?: number;
  lastFailure?: string;
}

export interface PoolWithID extends Pool {
  id: string;
}

export interface Block {
  valid: boolean;
  unlocked: boolean;
  ts: number;
  shares: number;
  pool_type: string;
  height: number;
  hash: string;
  diff: number;
  value: number;
}

export interface FullBlock extends Block {
  coin: string;
  poolId: string;
  poolName: string;
  LastUpdated: number;
  index: number;
  coinPoolType: string;
}

export interface Payment {
  fee: number;
  hash: string;
  id: number;
  mixins: number;
  payees: number;
  pool_type: string;
  ts: number;
  value: number;
}

export interface FullPayment extends Payment {
  coin: string;
  poolId: string;
  poolName: string;
  LastUpdated: number;
  index: number;
  coinPoolType: string;
}
