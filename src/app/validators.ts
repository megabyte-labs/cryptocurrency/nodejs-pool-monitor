import { FormControl } from '@angular/forms';

const validUrl = require('valid-url');

export const HTTPSValidator = (control: FormControl) => {
    const value = control.value;
    if (validUrl.isHttpsUri(value)) {
        return null;
    } else {
        return value;
    }
}