import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coinValue'
})
export class CoinValuePipe implements PipeTransform {

  transform(amount: number) {
    return amount / 1000000000000;
  }

}
