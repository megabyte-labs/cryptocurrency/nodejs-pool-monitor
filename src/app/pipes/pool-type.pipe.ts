import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'poolType'
})
export class PoolTypePipe implements PipeTransform {

  transform(value: any): any {
    if (value === 'pplns') {
      return 'PPLNS';
    } else if (value === 'solo') {
      return 'Solo';
    } else if (value === 'pps') {
      return 'PPS';
    } else {
      return 'Unknown';
    }
  }

}
