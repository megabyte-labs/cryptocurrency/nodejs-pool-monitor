import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textFilter'
})
export class TextFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const field = args[0];
    const filter = args[1]
    if (!value || !field || !filter) {
      return value;
    } else {
      const values = [];
      for (const entry of value) {
        if (entry[field].toLowerCase().indexOf(filter) !== -1) {
          values.push(entry);
        }
      }
      return values;
    }
  }

}
