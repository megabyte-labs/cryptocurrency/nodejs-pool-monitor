import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hash'
})
export class HashPipe implements PipeTransform {

  transform(hashes: number, totalHashes?: boolean) {
    if (hashes > 1000000000000000) {
      return Math.floor(hashes / 1000000000000) + 'TH';
    } else if (hashes > 1000000000000) {
      return Math.floor(hashes / 1000000000000) + '.' + (hashes % 1000000000000).toString().substring(0, 1) + (totalHashes ? 'TH' : 'TH/s');
    } else if (hashes > 1000000000) {
      return Math.floor(hashes / 1000000000) + '.' + (hashes % 1000000000).toString().substring(0, 1) + (totalHashes ? 'GH' : 'GH/s');
    } else if (hashes > 1000000) {
      return Math.floor(hashes / 1000000) + '.' + (hashes % 1000000).toString().substring(0, 1) + (totalHashes ? 'MH' : ' MH/s');
    } else if (hashes > 1000) {
      return Math.floor(hashes / 1000) + '.' + (hashes % 1000).toString().substring(0, 1) + (totalHashes ? 'KH' : ' KH/s');
    } else {
      return (hashes || 0) + ' H/s';
    }
  }

}
