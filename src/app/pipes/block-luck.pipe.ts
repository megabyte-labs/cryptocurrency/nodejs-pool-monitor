import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'blockLuck'
})
export class BlockLuckPipe implements PipeTransform {

  transform(luck: number): any {
    if (luck) {
      return 100 - luck;
    } else {
      return 0;
    }
  }

}
