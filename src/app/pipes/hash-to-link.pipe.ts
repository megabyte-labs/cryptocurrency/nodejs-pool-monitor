import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hashToLink'
})
export class HashToLinkPipe implements PipeTransform {

  transform(hash: string, ...args) {
    if (!hash || !args[0] || !args[1]) {
      return '';
    } else {
      const coin = args[0];
      const type = args[1];
      if (coin === 'XMR') {
        return 'https://moneroblocks.info/' + type + '/' + hash;
      } else if (coin === 'ETN') {
        return 'https://blockexplorer.electroneum.com/' + type + '/' + hash;
      } else if (coin === 'LOKI') {
        return 'https://lokiblocks.com/' + type + '/' + hash;
      } else if (coin === 'RYO') {
        return 'https://explorer.ryo-currency.com/' + type + '/' + hash;
      } else if (coin === 'SUMO') {
        return 'https://explorer.sumokoin.com/' + type + '/' + hash;
      } else if (coin === 'AEON') {
        return 'https://aeonblocks.com/' + type + '/' + hash;
      } else {
        return '';
      }
    }
  }

}
