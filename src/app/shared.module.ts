import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { APIErrorChartComponent } from './components/apierror-chart/apierror-chart.component';
import { CoinPriceChartComponent } from './components/coin-price-chart/coin-price-chart.component';
import { HeaderToolbarComponent } from './components/header-toolbar/header-toolbar.component';
import { LegalFooterComponent } from './components/legal-footer/legal-footer.component';
import { ModalComponent } from './components/modal/modal.component';
import { PoolStatsChartComponent } from './components/pool-stats-chart/pool-stats-chart.component';
import { BlockLuckPipe } from './pipes/block-luck.pipe';
import { CoinValuePipe } from './pipes/coin-value.pipe';
import { DifficultyToHashPipe } from './pipes/difficulty-to-hash.pipe';
import { FromNowPipe } from './pipes/from-now.pipe';
import { FullCurrencyNamePipe } from './pipes/full-currency-name.pipe';
import { HashPipe } from './pipes/hash.pipe';
import { HashToLinkPipe } from './pipes/hash-to-link.pipe';
import { PoolTypePipe } from './pipes/pool-type.pipe';
import { PrettyCurrencyPipe } from './pipes/pretty-currency.pipe';
import { SortByPipe } from './pipes/sort-by.pipe';
import { TextFilterPipe } from './pipes/text-filter.pipe';

@NgModule({
    declarations: [
        APIErrorChartComponent,
        CoinPriceChartComponent,
        HeaderToolbarComponent,
        LegalFooterComponent,
        ModalComponent,
        PoolStatsChartComponent,
        BlockLuckPipe,
        CoinValuePipe,
        DifficultyToHashPipe,
        FromNowPipe,
        FullCurrencyNamePipe,
        HashPipe,
        HashToLinkPipe,
        PoolTypePipe,
        PrettyCurrencyPipe,
        SortByPipe,
        TextFilterPipe
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    exports: [
        APIErrorChartComponent,
        CoinPriceChartComponent,
        HeaderToolbarComponent,
        LegalFooterComponent,
        ModalComponent,
        PoolStatsChartComponent,
        BlockLuckPipe,
        CoinValuePipe,
        DifficultyToHashPipe,
        FromNowPipe,
        FullCurrencyNamePipe,
        HashPipe,
        HashToLinkPipe,
        PoolTypePipe,
        PrettyCurrencyPipe,
        SortByPipe,
        TextFilterPipe
    ]
})
export class SharedModule { }
