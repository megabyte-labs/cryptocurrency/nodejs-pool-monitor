import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { APIErrorChartComponent } from './apierror-chart.component';

describe('APIErrorChartComponent', () => {
  let component: APIErrorChartComponent;
  let fixture: ComponentFixture<APIErrorChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ APIErrorChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(APIErrorChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
