import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { DataService } from '../../services/data.service';
import { HTTPSValidator } from '../../validators';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  poolSubmitForm;
  selectTokenOptions: any = {
    header: 'Select token',
    cssClass: 'inverted-colors'
  };

  constructor(public dataService: DataService, private formBuilder: FormBuilder) {
    const nameRegEx = '^[A-Za-z0-9][A-Za-z0-9 ]*$';
    this.poolSubmitForm = this.formBuilder.group({
      url: ['', Validators.compose([
        Validators.required,
        Validators.minLength(14),
        Validators.maxLength(50),
        HTTPSValidator
      ])],
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(24),
        Validators.pattern(nameRegEx)
      ])],
      coin: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  submitNewPool() {
    this.dataService.submitPool(this.poolSubmitForm.value.coin, this.poolSubmitForm.value.url, this.poolSubmitForm.value.name)
  }

}
