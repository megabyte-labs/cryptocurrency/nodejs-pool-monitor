import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoolStatsChartComponent } from './pool-stats-chart.component';

describe('PoolStatsChartComponent', () => {
  let component: PoolStatsChartComponent;
  let fixture: ComponentFixture<PoolStatsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoolStatsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolStatsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
