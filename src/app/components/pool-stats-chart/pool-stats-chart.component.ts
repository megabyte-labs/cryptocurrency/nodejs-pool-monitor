import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { BehaviorSubject, combineLatest } from 'rxjs';
import * as moment from 'moment';

import { DataService } from '../../services/data.service';
import { HashPipe } from '../../pipes/hash.pipe';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pool-stats-chart',
  templateUrl: './pool-stats-chart.component.html',
  styleUrls: ['./pool-stats-chart.component.scss'],
  providers: [HashPipe]
})
export class PoolStatsChartComponent implements OnInit, OnDestroy {
  @Input('color') color;
  @Input('title') title;
  @ViewChild('chartCanvas') canvas;
  private chart;
  private datasets = [];
  private history$;
  private range$: BehaviorSubject<'minute' | 'hour' | 'day'>;
  private render$: BehaviorSubject<boolean>;
  private yAxes = [];
  private _renderDelay;

  constructor(
    private dataService: DataService,
    private hashPipe: HashPipe
  ) {
    this.range$ = new BehaviorSubject<'minute' | 'hour' | 'day'>('minute');
    this.render$ = new BehaviorSubject(false);
    this.history$ = this.dataService.getPoolHistory(this.range$).subscribe((data: any[]) => {
      if (data) {
        this.datasets = [];
        for (const type of Object.keys(environment.poolCharts)) {
          const yAxes = {
            afterFit: (scale) => {
              if (scale.options.display) {
                if (type === 'hashRate' || type === 'totalBlocksFound' || type === 'totalPayments' || type === 'roundHashes') {
                  scale.width = 95;
                } else if (type === 'miners') {
                  scale.width = 69;
                } else if (type === 'totalHashes') {
                  scale.width = 85;
                } else if (type === 'totalMinersPaid') {
                  scale.width = 81;
                }
              }
            },
            id: type,
            display: type === 'miners' || type === 'hashRate',
            position: this.datasets.length ? 'right' : 'left',
            type: 'linear',
            scaleLabel: {
              display: true,
              labelString: environment.poolCharts[type].ylabel,
              fontColor: '#dedede',
              fontFamily: '"Montserrat", -apple-system, sans-serif',
              fontSize: 16,
              fontStyle: 'bold',
              padding: type === 'hashRate' ? 0 : 8
            },
            ticks: {
              callback: (value) => {
                if (type === 'hashRate') {
                  return this.hashPipe.transform(value);
                } else if (type === 'totalHashes' || type === 'roundHashes') {
                  return this.hashPipe.transform(value, true);
                } else if (type === 'miners' || type === 'totalMinersPaid') {
                  return parseInt(value, 10);
                } else {
                  return value.toPrecision(4);
                }
              },
              fontColor: 'rgba(255,255,255,0.667)',
              padding: 0
            }
          };
          this.yAxes.push(yAxes);
          const dataset = {
            label: environment.poolCharts[type].label,
            data: data.map(item => { return { x: new Date(item.id), y: item[type] } }),
            yAxisID: type,
            backgroundColor: environment.chartColors[this.datasets.length],
            showLine: true,
            hidden: type !== 'miners' && type !== 'hashRate'
          };
          this.datasets.push(dataset);
        }
        this.render$.next(true);
      }
    });
  }


  ngOnInit() {
    this.render$.subscribe(shouldRender => {
      if (shouldRender) {
        this.renderChart();
      }
    });
  }

  renderChart() {
    if (typeof this.chart !== 'undefined' && this.chart.animating) {
      this._renderDelay = setTimeout(() => {
        this.renderChart();
      }, 100);
    } else {
      if (!this.chart) {
        this.initChart();
      } else {
        this.chart.update({
          duration: 333
        });
      }
    }
  }

  initChart() {
    const data = {
      datasets: this.datasets
    };
    const options = {
      legend: {
        display: true,
        labels: {
          fontColor: '#dedede',
          fontFamily: '"Montserrat", -apple-system, sans-serif',
          fontStyle: 'bold'
        },
        onClick: (event, value) => {
          if (value.hidden) {
            this.chart.data.datasets[value.datasetIndex].hidden = false;
            this.chart.options.scales.yAxes[value.datasetIndex].display = true;
          } else {
            this.chart.data.datasets[value.datasetIndex].hidden = true;
            this.chart.options.scales.yAxes[value.datasetIndex].display = false;
          }
          this.chart.update();
        }
      },
      tooltips: {
        bodyFontSize: 21,
        boxWidth: 50,
        displayColors: false,
        bodyFontStyle: 'bold',
        callbacks: {
          label: (tooltipItem) => {
            return tooltipItem.yLabel;
          },
          title: (data) => {
            console.log(data);
            return moment(data[0].xLabel).format('dddd hA') + ' (' + moment(data[0].xLabel).fromNow() + ')';
          }
        }
      },
      resoponsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          type: 'time',
          distribution: 'linear',
          ticks: {
            fontColor: 'rgba(255,255,255,0.667)'
          },
          time: {
            displayFormats: {
              hour: 'MMM D h:mm a'
            },
            stepSize: 2,
            unit: 'hour'
          }
        }],
        yAxes: this.yAxes
      }
    };
    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'scatter',
      data: data,
      options: options
    });
  }

  ngOnDestroy() {
    clearTimeout(this._renderDelay);
    if (this.history$) { this.history$.unsubscribe(); }
  }

}
