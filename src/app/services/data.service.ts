import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, throwError, combineLatest, Subject } from 'rxjs';
import { map, catchError, throttleTime, switchMap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Coin, CoinWithID, Pool, PoolWithID, Block, FullBlock, Payment, FullPayment } from '../constants/interfaces';
import { database } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public coin = null;
  public coin$: BehaviorSubject<string | null>;
  public pool = null;
  public pool$: BehaviorSubject<string | null>;
  public type = null;
  public type$: BehaviorSubject<string | null>;
  public blockOrderBy = 'ts';
  public blockOrderBy$: BehaviorSubject<string | null>;
  public blockOrderByAsc: any = false;
  public blockOrderByAsc$: BehaviorSubject<boolean>;
  public limit = 50;
  public limit$: BehaviorSubject<number>;
  public coins: CoinWithID[];
  public coinArray = [];
  private coinArrayAcquired = false;
  public coinPrices = {};
  public coinTwentyFourPositive = {};
  public coinNameTable = {};
  public pools: PoolWithID[];
  public poolTable = {};
  public poolIdTable = {};
  private poolTableAcquired = false;
  public poolStatus = {};
  public poolHashRate = {};
  public sideMenuPoolsToggled = {};
  public initFinished$: BehaviorSubject<boolean>;
  public updateCoin$: BehaviorSubject<boolean>;
  public updatePool$: BehaviorSubject<boolean>;

  constructor(private afs: AngularFirestore, private httpClient: HttpClient, public storage: Storage) {
    this.coin$ = new BehaviorSubject(null);
    this.pool$ = new BehaviorSubject(null);
    this.type$ = new BehaviorSubject(null);
    this.blockOrderBy$ = new BehaviorSubject('ts');
    this.blockOrderByAsc$ = new BehaviorSubject(false);
    this.limit$ = new BehaviorSubject(50);
    this.initFinished$ = new BehaviorSubject(false);
    this.updatePool$ = new BehaviorSubject(false);
    this.updateCoin$ = new BehaviorSubject(false);
  }

  initAppDefaultSubs() {
    this.afs.collection<Coin>('coins').snapshotChanges().pipe(
      throttleTime(5000),
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data() as Coin;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(data => {
      this.coins = data;
      for (const coin of this.coins) {
        this.coinPrices[coin.id] = coin.usdPrice;
        this.coinNameTable[coin.name.toLowerCase()] = coin;
        this.coinTwentyFourPositive[coin.id] = coin.changePercent24Hour > 0 ? true : false;
      }
      if (!this.coinArrayAcquired) {
        this.coinArray = data;
        this.coinArrayAcquired = true;
        if (this.poolTableAcquired) {
          this.initFinished$.next(true);
        }
      }
      this.updateCoin$.next(true);
    });
    this.afs.collection<Pool>('pools').snapshotChanges().pipe(
      throttleTime(5000),
      map((actions: any) => actions.map(a => {
        const data = a.payload.doc.data() as Pool;
        const id = a.payload.doc.id;
        const decodedId = decodeURIComponent(id);
        return { id, decodedId, ...data };
      }))
    ).subscribe(data => {
      this.pools = data;
      for (const pool of this.pools) {
        const lastFailure: number = parseInt(pool.lastFailure, 10);
        this.poolStatus[pool.id] = new Date().getTime() - lastFailure < 1440000 ? true : false;
        this.poolHashRate[pool.id] = pool.hashRatePool;
        this.poolIdTable[pool.id.toLowerCase()] = pool;
        if (!this.poolTableAcquired) {
          if (!this.poolTable[pool.coin]) { this.poolTable[pool.coin] = []; }
          this.poolTable[pool.coin].push(pool);
        }
      }
      this.poolTableAcquired = true;
      if (this.coinArrayAcquired) {
        this.initFinished$.next(true);
      }
      this.updatePool$.next(true);
    });
  }

  getPoolHistory(
    range$: BehaviorSubject<'minute' | 'hour' | 'day'>
  ) {
    return combineLatest(
      this.pool$,
      this.type$,
      range$
    ).pipe(
      switchMap(([pool, type, range]): any => {
        if (pool && range) {
          if (!type) { type = 'Pool'; }
          return this.afs.collection('pools').doc(pool).collection('statsHistory' + type, ref => {
            let query: any = ref;
            if (range === 'hour') { query = query.where('hourTick', '==', true); }
            if (range === 'day') { query = query.where('dayTick', '==', true); }
            query = query.limit(240);
            return query;
          }).snapshotChanges().pipe(
            map(actions =>
              actions.map(a => {
                const data = a.payload.doc.data();
                const id = parseInt(a.payload.doc.id, 10);
                return { id, ...data };
              })
            )
          );
        } else {
          console.error('No pool or range, or point declared with getPoolHistory()')
        }
      }));
  }

  getBlocks() {
    return combineLatest(
      this.type$,
      this.pool$,
      this.coin$,
      this.blockOrderBy$,
      this.blockOrderByAsc$,
      this.limit$
    ).pipe(
      switchMap(([type, pool, coin, orderBy, asc, limit]): any => {
        this.coin = coin;
        this.pool = pool;
        this.type = type;
        this.blockOrderBy = orderBy;
        this.blockOrderByAsc = asc;
        this.limit = limit;
        if (type && coin) {
          if (pool) {
            return this.afs.collection<Block>('pools').doc(pool).collection('blocks', ref => {
              let query: any = ref;
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          } else {
            return this.afs.collection<FullBlock>('blocks', ref => {
              let query: any = ref;
              query = query.where('coinPoolType', '==', coin + '-' + type);
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          }
        } else {
          if (pool) {
            return this.afs.collection<Block>('pools').doc(pool).collection('blocks', ref => {
              let query: any = ref;
              if (type) {
                query = query.where('pool_type', '==', type);
              }
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          } else {
            return this.afs.collection<FullBlock>('blocks', ref => {
              let query: any = ref;
              if (type) {
                query = query.where('pool_type', '==', type);
              } else if (coin) {
                query = query.where('coin', '==', coin);
              }
              if (orderBy) {
                query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              }
              query = query.limit(limit);
              return query;
            }).valueChanges();
          }
        }
      })
    );
  }

  getPayments() {
    return combineLatest(
      this.type$,
      this.pool$,
      this.coin$,
      this.blockOrderBy$,
      this.blockOrderByAsc$,
      this.limit$
    ).pipe(
      switchMap(([type, pool, coin, orderBy, asc, limit]): any => {
        this.coin = coin;
        this.pool = pool;
        this.type = type;
        this.blockOrderBy = orderBy;
        this.blockOrderByAsc = asc;
        this.limit = limit;
        if (type && coin) {
          if (pool) {
            return this.afs.collection<Payment>('pools').doc(pool).collection('payments', ref => {
              let query: any = ref;
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          } else {
            return this.afs.collection<FullPayment>('payments', ref => {
              let query: any = ref;
              query = query.where('coinPoolType', '==', coin + '-' + type);
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          }
        } else {
          if (pool) {
            return this.afs.collection<Payment>('pools').doc(pool).collection('payments', ref => {
              let query: any = ref;
              if (type) {
                query = query.where('pool_type', '==', type);
              }
              query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              query = query.limit(limit);
              return query;
            }).valueChanges();
          } else {
            return this.afs.collection<FullPayment>('payments', ref => {
              let query: any = ref;
              if (type) {
                query = query.where('pool_type', '==', type);
              } else if (coin) {
                query = query.where('coin', '==', coin);
              }
              if (orderBy) {
                query = query.orderBy(orderBy, asc ? 'asc' : 'desc');
              }
              query = query.limit(limit);
              return query;
            }).valueChanges();
          }
        }
      })
    );
  }

  getPorts(pool) {
    return this.afs.collection('pools').doc(pool).collection('ports').doc('config').valueChanges();
  }

  filterBy(type: string, filter) {
    this[type + '$'].next(filter);
  }

  submitPool(coin: string, url: string, name: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient.post(environment.endpoint.submitPool, { coin: coin, url: url, name: name }, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    console.log('Error deets', error);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
