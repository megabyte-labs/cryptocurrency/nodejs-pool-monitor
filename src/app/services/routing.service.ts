import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {
  private _blockSegment = 'explorer';
  private _dashboardSegment = 'overview';
  private _paymentSegment = 'explorer';
  private _poolSegment = 'dashboard';
  private _settingSegment = 'general';
  public params: any = {};
  private segments = ['_blockSegment', '_dashboardSegment', '_paymentSegment', '_poolSegment', '_settingSegment'];

  constructor(
    private dataService: DataService,
    public location: Location,
    public route: ActivatedRoute,
    public router: Router,
    private storage: Storage
  ) {
    this.init();
  }

  init() {
    this.route.params.subscribe(params => {
      this.params = params;
      if (params.pool) {
        this.dataService.updatePool$.subscribe(shouldProcess => {
          if (shouldProcess) {
            this.dataService.sideMenuPoolsToggled[this.dataService.poolIdTable[params.pool.toLowerCase()].coin] = true;
          }
        });
      }
      const type = this.route.snapshot.data['type'];
      let ignore;
      if (type && params.segment) {
        ignore = '_' + type + 'Segment';
        this.storage.set(ignore, params.segment);
        this[ignore] = params.segment;
      }
      for (const segment of this.segments) {
        this.storage.get(segment).then((data) => {
          if (segment !== ignore) {
            if (data) {
              this[segment] = data;
            } else {
              this.storage.set(segment, this[segment]);
            }
          }
        });
      }
    });
  }

  get blockSegment(): string {
    return this._blockSegment;
  }

  set blockSegment(segment: string) {
    this._blockSegment = segment;
    this.storage.set('_blockSegment', segment);
  }

  get dashboardSegment(): string {
    return this._dashboardSegment;
  }

  set dashboardSegment(segment: string) {
    this._dashboardSegment = segment;
    this.storage.set('_dashboardSegment', segment);
  }

  get paymentSegment(): string {
    return this._paymentSegment;
  }

  set paymentSegment(segment: string) {
    this._paymentSegment = segment;
    this.storage.set('_paymentSegment', segment);
  }

  get poolSegment(): string {
    return this._poolSegment;
  }

  set poolSegment(segment: string) {
    this._poolSegment = segment;
    this.storage.set('_poolSegment', segment);
  }

  get settingSegment(): string {
    return this._settingSegment;
  }

  set settingSegment(segment: string) {
    this._settingSegment = segment;
    this.storage.set('_settingSegment', segment);
  }

  changePage(path: string) {
    this.router.navigateByUrl(path);
  }
}
