import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';

interface AlertData {
  title: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  constructor(
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private toastController: ToastController
  ) { }

  async actionSheet(data) {
    const { Device, Modals } = Plugins;
    const info = await Device.getInfo();
    if (info.platform === 'ios' || info.platform === 'android') {
      // https://capacitor.ionicframework.com/docs/apis/modals
      const nativeData: any = data;
      Modals.showActions(nativeData);
    } else {
      const actionSheet = await this.actionSheetController.create(data);
      await actionSheet.present();
    }
  }

  async alert(data: AlertData) {
    const { Device, Modals } = Plugins;
    const info = await Device.getInfo();
    console.log(info);
    if (info.platform === 'ios' || info.platform === 'android') {
      Modals.alert({
        title: data.title,
        message: data.message
      });
    } else {
      const alert = await this.alertController.create({
        header: data.title,
        message: data.message,
        buttons: ['OK']
      });
      await alert.present();
    }
  }

  async toast(data: string, position?: 'top' | 'middle' | 'bottom', duration?: number) {
    const { Device, Toast } = Plugins;
    const info = await Device.getInfo();
    if (info.platform === 'ios' || info.platform === 'android') {
      await Toast.show({
        text: data
      });
    } else {
      const toast = await this.toastController.create({
        message: data,
        position: position ? position : 'middle',
        showCloseButton: true,
        duration: duration ? duration : 10000
      });
      toast.present();
    }
  }
}
