import { Injectable } from '@angular/core';
import * as Butter from 'buttercms';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ButterCMSService {
  butterService;

  constructor() {
    this.butterService = Butter(environment.butter.api_key);
  }

  async getPage(type: string, page: string) {
    const data = await this.butterService.page.retrieve(type, page);
    return data.data.data.fields;
  }
}
