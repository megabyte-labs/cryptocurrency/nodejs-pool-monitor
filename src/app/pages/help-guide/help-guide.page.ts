import { Component, OnInit } from '@angular/core';

import { ButterCMSService } from '../../services/butter-cms.service';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-help-guide',
  templateUrl: './help-guide.page.html',
  styleUrls: ['./help-guide.page.scss'],
  providers: [RoutingService]
})
export class HelpGuidePage implements OnInit {
  lastModified;
  pageContent;

  constructor(private butterCMS: ButterCMSService) { }

  async ngOnInit() {
    const data = await this.butterCMS.getPage('generic', 'help');
    this.pageContent = data.page_content;
    this.lastModified = data.last_edited;
  }

}
