import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpGuidePage } from './help-guide.page';

describe('HelpGuidePage', () => {
  let component: HelpGuidePage;
  let fixture: ComponentFixture<HelpGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpGuidePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
