import { Component, OnInit } from '@angular/core';

import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
  providers: [RoutingService]
})
export class ChatPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
