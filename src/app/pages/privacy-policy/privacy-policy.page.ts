import { Component, OnInit } from '@angular/core';

import { ButterCMSService } from '../../services/butter-cms.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})
export class PrivacyPolicyPage implements OnInit {
  lastModified;
  pageContent;

  constructor(private butterCMS: ButterCMSService) { }

  async ngOnInit() {
    const data = await this.butterCMS.getPage('generic', 'privacy-policy');
    this.pageContent = data.page_content;
    this.lastModified = data.last_edited;
  }

}
