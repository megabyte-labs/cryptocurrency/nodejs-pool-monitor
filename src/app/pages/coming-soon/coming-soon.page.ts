import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ComponentsService } from '../../services/components.service';
import { EmailService } from '../../services/email.service';

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.page.html',
  styleUrls: ['./coming-soon.page.scss'],
})
export class ComingSoonPage {
  newsletterForm;

  constructor(private componentsService: ComponentsService, private emailService: EmailService, private formBuilder: FormBuilder) {
    this.newsletterForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  submitEmail() {
    this.emailService.subscribeToNewsletter(this.newsletterForm.value.email).subscribe(async () => {
      this.componentsService.alert({
        title: 'Successfully Subscribed',
        message: 'You have been successfully subscribed to receive updates and an invite in the near future for an early-access preview.'
      });
    }, (error) => {
      let message = 'There was an error subscribing. Try using another e-mail address.';
      if (error.error && error.error.error === 'Member Exists') {
        message = 'That e-mail address is already subscribed.';
      }
      this.componentsService.alert({
        title: 'Failed to Subscribe',
        message: message
      });
    });
  }

}
