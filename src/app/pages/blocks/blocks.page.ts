import { Component, OnInit, OnDestroy } from '@angular/core';

import { ComponentsService } from '../../services/components.service';
import { DataService } from '../../services/data.service';
import { FullBlock } from '../../constants/interfaces';
import { FullCurrencyNamePipe } from '../../pipes/full-currency-name.pipe';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.page.html',
  styleUrls: ['./blocks.page.scss'],
  providers: [FullCurrencyNamePipe, RoutingService]
})
export class BlocksPage implements OnInit, OnDestroy {
  blocks = [];
  blocks$;
  blockHashTable: any = {};
  blockFields = ['diff', 'valid', 'unlocked', 'ts', 'shares', 'height', 'diff', 'value'];
  obtainedBlocks = false;
  segment;
  filterOptions: any = {
    header: 'Add Filter',
    cssClass: 'inverted-colors'
  };

  constructor(
    private components: ComponentsService,
    private currencyName: FullCurrencyNamePipe,
    private dataService: DataService,
    public routing: RoutingService
  ) { }

  processBlockField(block: FullBlock, oldBlock: FullBlock, field: string) {
    const time = new Date().getTime();
    if (block[field] !== oldBlock[field]) {
      this.blockHashTable[block.hash][field] = block[field];
      const index = this.blockHashTable[block.hash].index;
      this.blocks[index][field] = block[field];
      this.blocks[index][field + 'LastUpdated'] = time;
    }
  }

  hasChanged(field: string, block: FullBlock) {
    const time = new Date().getTime();
    if (block && block[field + 'LastUpdated'] && time - block[field + 'LastUpdated'] < 60000) {
      return true;
    } else {
      return false;
    }
  }

  trackByFn(index, item) {
    return item.id;
  }

  ngOnInit() {
    this.blocks$ = this.dataService.getBlocks().subscribe((data: FullBlock[]) => {
      console.log(data);
      let counter = 0;
      const time = new Date().getTime();
      for (const block of data) {
        const oldBlock = this.blockHashTable[block.hash];
        if (!oldBlock) {
          if (this.blocks[counter] && this.blocks[counter].hash !== data[counter].hash) {
            // Something was added to beginning
            block.LastUpdated = time;
            this.blocks.unshift(block);
            for (const [key, value] of <any>Object.entries(this.blockHashTable)) {
              this.blockHashTable[key].index = value.index + 1;
            }
            const hashValue: any = block;
            hashValue.index = 0;
            this.blockHashTable[block.hash] = hashValue;
          } else {
            this.blocks.push(block);
            const hashValue: any = block;
            hashValue.index = counter;
            this.blockHashTable[block.hash] = hashValue;
          }
        } else {
          for (const field of this.blockFields) {
            this.processBlockField(block, oldBlock, field);
          }
        }
        counter++;
      }
      this.obtainedBlocks = this.blocks.length > 0;
    });
  }

  addFilter(field: string) {
    let actionSheetData;
    const actionSheetButtons = [];
    const cancelButton = {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel'
    };
    if (field === 'pool_type') {
      actionSheetData = {
        header: 'Filter by pool type'
      };
      const poolTypes = ['PPLNS', 'Solo', 'PPS'];
      for (const poolType of poolTypes) {
        actionSheetButtons.push({
          text: poolType,
          icon: poolType === 'Solo' ? 'person' : 'people',
          handler: () => {
            this.blocks = [];
            this.blockHashTable = {};
            this.dataService.type$.next(poolType.toLowerCase());
          }
        });
      }
    } else if (field === 'coin') {
      actionSheetData = {
        header: 'Filter by token'
      };
      for (const coin of this.dataService.coinArray) {
        actionSheetButtons.push({
          text: coin.name + ' (' + coin.id + ')',
          icon: 'custom-icon',
          cssClass: 'custom-icon-' + encodeURIComponent(coin.name.toLowerCase()),
          handler: () => {
            this.blocks = [];
            this.blockHashTable = {};
            this.dataService.coin$.next(coin.id);
          }
        });
      }

    } else if (field === 'poolName') {
      actionSheetData = {
        header: 'Select a pool'
      };
      for (const pool of this.dataService.pools) {
        actionSheetButtons.push({
          text: pool.name + ' (' + pool.coin + ')',
          icon: 'ios-analytics',
          handler: () => {
            this.routing.changePage('/pool/' + this.currencyName.transform(pool.coin).toLowerCase() + '/' + encodeURIComponent(pool.id).toLowerCase() + '/blocks');
          }
        });
      }
    } else {
      console.error('Unrecognized field at addFilter() in blocks.page.ts');
    }
    actionSheetButtons.push(cancelButton);
    actionSheetData.buttons = actionSheetButtons;
    this.components.actionSheet(actionSheetData);
  }

  removeFilter(type: string) {
    if (type === 'pool_type') {
      this.blocks = [];
      this.blockHashTable = {};
      this.dataService.type$.next(null);
    } else if (type === 'coin') {
      this.blocks = [];
      this.blockHashTable = {};
      this.dataService.coin$.next(null);
    } else {
      console.error('Unrecognized field at removeFilter() in blocks.page.ts');
    }
  }

  segmentChanged(event) {
    this.routing.location.go('/blocks/' + event.detail.value);
  }

  changeFilter(filter: string) {
    this.blocks = [];
    this.blockHashTable = {};
    if (filter === this.dataService.blockOrderBy) {
      this.dataService.blockOrderByAsc$.next(!this.dataService.blockOrderByAsc);
    } else {
      this.dataService.blockOrderBy$.next(filter);
    }
  }

  ngOnDestroy() {
    if (this.blocks$) { this.blocks$.unsubscribe(); }
  }

}
