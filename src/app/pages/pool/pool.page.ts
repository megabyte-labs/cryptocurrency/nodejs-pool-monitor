import { Component, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ComponentsService } from '../../services/components.service';
import { DataService } from '../../services/data.service';
import { RoutingService } from '../../services/routing.service';
import { FullBlock, FullPayment } from '../../constants/interfaces';
import { FullCurrencyNamePipe } from '../../pipes/full-currency-name.pipe';
import { PoolTypePipe } from '../../pipes/pool-type.pipe';
import { SortByPipe } from '../../pipes/sort-by.pipe';
import { TextFilterPipe } from '../../pipes/text-filter.pipe';

@Component({
  selector: 'app-pool',
  templateUrl: './pool.page.html',
  styleUrls: ['./pool.page.scss'],
  providers: [FullCurrencyNamePipe, PoolTypePipe, SortByPipe, TextFilterPipe, RoutingService]
})
export class PoolPage implements OnInit, OnDestroy {
  blocks = [];
  blocks$;
  blockFields = ['diff', 'valid', 'unlocked', 'ts', 'shares', 'height', 'value', 'luck', 'hash'];
  blockHashTable = {};
  coin;
  coinNameLower;
  coin$;
  infinite;
  init$;
  innerWidth;
  networkHeight = 0;
  obtainedBlocks = false;
  obtainedPayments = false;
  obtainedPorts = false;
  paymentFields = ['ts', 'hash', 'value', 'payees', 'mixins', 'hash'];
  paymentHashTable = {};
  payments = [];
  payments$;
  pool;
  pool$;
  poolLowerId;
  portOrderBy = 'miners';
  portOrderByAsc = false;
  portFields = ['port', 'type', 'miners', 'ip', 'hostname', 'blockIDTime', 'blockID', 'difficulty', 'description'];
  ports = [];
  portHashTable = {};
  ports$;
  ready$;
  route$;
  segment = 'dashboard';
  isReady$: BehaviorSubject<boolean>;

  constructor(
    private components: ComponentsService,
    private currencyName: FullCurrencyNamePipe,
    private poolType: PoolTypePipe,
    public dataService: DataService,
    public routing: RoutingService,
    private sortBy: SortByPipe,
    private textFilter: TextFilterPipe
  ) {
    this.innerWidth = window.innerWidth;
    this.isReady$ = new BehaviorSubject(false);
    this.route$ = this.routing.route.params.subscribe(params => {
      this.segment = params.segment;
      this.poolLowerId = params.pool.toLowerCase();
      this.coinNameLower = params.coin.toLowerCase();
      if (this.init$) { this.init$.unsubscribe(); }
      let hasInit = false;
      this.init$ = this.dataService.initFinished$.subscribe(finished => {
        if (finished) {
          if (this.coin$) { this.coin$.unsubscribe(); }
          this.coin$ = this.dataService.updateCoin$.subscribe(shouldProcess => {
            if (shouldProcess) {
              this.coin = this.dataService.coinNameTable[this.coinNameLower];
              this.networkHeight = this.coin.networkHeight ? this.coin.networkHeight : 0;
              if (this.pool && !hasInit) {
                this.isReady$.next(true);
                hasInit = true;
                console.log(this.pool);
              }
            }
          });
          if (this.pool$) { this.pool$.unsubscribe(); }
          this.pool$ = this.dataService.updatePool$.subscribe(shouldProcess => {
            if (shouldProcess) {
              this.pool = this.dataService.poolIdTable[this.poolLowerId];
              this.dataService.pool$.next(this.pool.id);
              if (this.coin && !hasInit) {
                this.isReady$.next(true);
                hasInit = true;
                console.log(this.pool);
              }
            }
          });
        }
      });
    });

  }

  loadMore(event) {
    this.infinite = event.target;
    const limit = this.dataService.limit + 50 >= 1000 ? 1000 : this.dataService.limit + 50;
    if (limit === 1000) {
      this.components.toast('Data tables are limited to 1000 records. Infinite scrolling has been disabled.', 'bottom');
    }
    this.dataService.limit$.next(limit);
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (this.segment === 'blocks') {
      this.handleSegment('blocks');
    } else if (this.segment === 'payments') {
      this.handleSegment('payments');
    } else if (this.segment === 'ports') {
      this.handleSegment('ports');
    } else {
      this.routing.poolSegment = 'dashboard';
      this.handleSegment('dashboard');
    }
  }

  processBlockField(block: any, oldBlock: any, field: string, type: string) {
    const time = new Date().getTime();
    if (block[field] !== oldBlock[field]) {
      this[type + 'HashTable'][block.hash][field] = block[field];
      const index = this[type + 'HashTable'][block.hash].index;
      this[type + 's'][index][field] = block[field];
      this[type + 's'][index][field + 'LastUpdated'] = time;
    }
  }

  hasChanged(field: string, block: FullBlock) {
    const time = new Date().getTime();
    if (block && block[field + 'LastUpdated'] && time - block[field + 'LastUpdated'] < 5000) {
      return true;
    } else {
      return false;
    }
  }

  loadBlocks() {
    if (this.blocks$) { this.blocks$.unsubscribe(); }
    this.blocks$ = this.dataService.getBlocks().subscribe((data: FullBlock[]) => {
      console.log(data);
      let counter = 0;
      const time = new Date().getTime();
      for (const block of data) {
        const oldBlock = this.blockHashTable[block.hash];
        if (!oldBlock) {
          if (this.blocks[counter] && this.blocks[counter].hash !== data[counter].hash) {
            // Something was added to beginning
            block.LastUpdated = time;
            this.blocks.unshift(block);
            for (const [key, value] of <any>Object.entries(this.blockHashTable)) {
              this.blockHashTable[key].index = value.index + 1;
            }
            const hashValue: any = block;
            hashValue.index = 0;
            this.blockHashTable[block.hash] = hashValue;
          } else {
            this.blocks.push(block);
            const hashValue: any = block;
            hashValue.index = counter;
            this.blockHashTable[block.hash] = hashValue;
          }
        } else {
          for (const field of this.blockFields) {
            this.processBlockField(block, oldBlock, field, 'block');
          }
        }
        counter++;
      }
      this.obtainedBlocks = this.blocks.length > 0;
      if (this.dataService.limit === this.blocks.length && this.infinite) {
        this.infinite.complete();
        this.infinite = null;
      }
    });
  }

  loadPayments() {
    if (this.payments$) { this.payments$.unsubscribe(); }
    this.payments$ = this.dataService.getPayments().subscribe((data: FullPayment[]) => {
      console.log(data);
      let counter = 0;
      const time = new Date().getTime();
      for (const payment of data) {
        const oldPayment = this.paymentHashTable[payment.hash];
        if (!oldPayment) {
          if (this.payments[counter] && this.payments[counter].hash !== data[counter].hash) {
            // Something was added to beginning
            payment.LastUpdated = time;
            this.payments.unshift(payment);
            for (const [key, value] of <any>Object.entries(this.paymentHashTable)) {
              this.paymentHashTable[key].index = value.index + 1;
            }
            const hashValue: any = payment;
            hashValue.index = 0;
            this.paymentHashTable[payment.hash] = hashValue;
          } else {
            this.payments.push(payment);
            const hashValue: any = payment;
            hashValue.index = counter;
            this.paymentHashTable[payment.hash] = hashValue;
          }
        } else {
          for (const field of this.paymentFields) {
            this.processBlockField(payment, oldPayment, field, 'payment');
          }
        }
        counter++;
      }
      this.obtainedPayments = this.payments.length > 0;
      if (this.dataService.limit === this.payments.length && this.infinite) {
        this.infinite.complete();
        this.infinite = null;
      }
    });
  }

  processPortData(data) {
    const ports = [];
    for (const key of Object.keys(data)) {
      for (const port of data[key]) {
        const portData: any = {};
        if (port.description) {
          portData.description = port.description;
        }
        if (port.difficulty) {
          portData.difficulty = parseInt(port.difficulty, 10);
        }
        if (port.host) {
          if (port.host.blockID) {
            portData.blockID = parseInt(port.host.blockID, 10);
          }
          if (port.host.blockIDTime) {
            portData.blockIDTime = parseInt(port.host.blockIDTime, 10) * 1000;
          }
          if (port.host.hostname) {
            portData.hostname = port.host.hostname;
          }
          if (port.host.ip) {
            portData.ip = port.host.ip;
          }
        }
        if (port.miners) {
          portData.miners = parseInt(port.miners, 10);
        } else {
          portData.miners = 0;
        }
        if (port.port) {
          portData.port = parseInt(port.port, 10);
        }
        if (port.pool_type && key === 'global') {
          portData.type = this.poolType.transform(port.pool_type) + ' (Global)';
        } else {
          portData.type = this.poolType.transform(key);
        }
        let hash;
        if (portData.hostname && portData.port) {
          hash = portData.hostname + portData.port;
        } else if (portData.ip && portData.port) {
          hash = portData.ip + portData.port;
        } else {
          continue;
        }
        portData.hash = hash;
        ports.push(portData);
      }
    }
    return ports;
  }

  loadPorts() {
    if (this.ports$) { this.ports$.unsubscribe(); }
    this.ports$ = this.dataService.getPorts(this.pool.id).subscribe(data => {
      if (data) {
        const ports = this.sortBy.transform(
          this.textFilter.transform(
            this.processPortData(data), ['type', this.dataService.type]
          ), [this.portOrderBy, this.portOrderByAsc]
        );
        let counter = 0;
        const time = new Date().getTime();
        for (const port of ports) {
          const oldPort = this.portHashTable[port.hash];
          if (!oldPort) {
            if (this.ports[counter] && this.ports[counter].hash !== ports[counter].hash) {
              // Something was added to beginning
              port.LastUpdated = time;
              this.ports.unshift(port);
              for (const [key, value] of <any>Object.entries(this.portHashTable)) {
                this.portHashTable[key].index = value.index + 1;
              }
              const hashValue: any = port;
              hashValue.index = 0;
              this.portHashTable[port.hash] = hashValue;
            } else {
              this.ports.push(port);
              const hashValue: any = port;
              hashValue.index = counter;
              this.portHashTable[port.hash] = hashValue;
            }
          } else {
            for (const field of this.portFields) {
              this.processBlockField(port, oldPort, field, 'port');
            }
          }
          counter++;
        }
      }
    });
  }

  addFilter(field: string) {
    let actionSheetData;
    const actionSheetButtons = [];
    const cancelButton = {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel'
    };
    if (field === 'pool_type') {
      actionSheetData = {
        header: 'Filter by pool type'
      };
      const poolTypes = ['PPLNS', 'Solo', 'PPS'];
      for (const poolType of poolTypes) {
        actionSheetButtons.push({
          text: poolType,
          icon: poolType === 'Solo' ? 'person' : 'people',
          handler: () => {
            this.blocks = [];
            this.blockHashTable = {};
            this.payments = [];
            this.paymentHashTable = {};
            this.dataService.blockOrderBy$.next('ts');
            this.dataService.blockOrderByAsc$.next(false);
            this.dataService.type$.next(poolType.toLowerCase());
            this.dataService.type = poolType.toLowerCase();
          }
        });
      }
    } else if (field === 'poolName') {
      actionSheetData = {
        header: 'Select a pool'
      };
      for (const pool of this.dataService.pools) {
        if (pool.id !== this.pool.id) {
          actionSheetButtons.push({
            text: pool.name + ' (' + pool.coin + ')',
            icon: 'ios-analytics',
            handler: () => {
              if (this.blocks$) { this.blocks$.unsubscribe(); }
              if (this.payments$) { this.payments$.unsubscribe(); }
              if (this.ready$) { this.ready$.unsubscribe(); }
              this.blocks = [];
              this.blockHashTable = {};
              this.payments = [];
              this.paymentHashTable = {};
              this.dataService.pool$.next(pool.id);
              if (this.dataService.coin !== pool.coin) {
                this.dataService.coin$.next(pool.coin);
              }
              console.log(this.segment);
              this.routing.changePage(
                '/pool/' +
                this.currencyName.transform(pool.coin).toLowerCase() +
                '/' +
                encodeURIComponent(pool.id).toLowerCase() +
                '/' +
                this.segment
              );
            }
          });
        }
      }
    } else {
      console.error('Unrecognized field at addFilter() in blocks.page.ts');
    }
    actionSheetButtons.push(cancelButton);
    actionSheetData.buttons = actionSheetButtons;
    this.components.actionSheet(actionSheetData);
  }

  changeFilter(filter: string) {
    this.blocks = [];
    this.blockHashTable = {};
    this.payments = [];
    this.paymentHashTable = {};
    if (this.dataService.limit !== 50) {
      this.dataService.limit$.next(50);
    }
    if (filter === this.dataService.blockOrderBy) {
      this.dataService.blockOrderByAsc$.next(!this.dataService.blockOrderByAsc);
    } else {
      this.dataService.blockOrderBy$.next(filter);
    }
  }

  changePortFilter(filter: string) {
    if (this.portOrderBy === filter) {
      this.portOrderByAsc = !this.portOrderByAsc;
    } else {
      this.portOrderByAsc = false;
      this.portOrderBy = filter;
    }
  }

  removeFilter(type: string) {
    if (type === 'pool_type') {
      this.blocks = [];
      this.blockHashTable = {};
      this.payments = [];
      this.paymentHashTable = {};
      this.dataService.type$.next(null);
      this.dataService.type = null;
    } else {
      console.error('Unrecognized field at removeFilter() in blocks.page.ts');
    }
  }

  handleSegment(segment: string) {
    this.blocks = [];
    this.blockHashTable = {};
    this.payments = [];
    this.paymentHashTable = {};
    this.obtainedBlocks = false;
    this.obtainedPayments = false;
    if (this.blocks$) { this.blocks$.unsubscribe(); }
    if (this.payments$) { this.payments$.unsubscribe(); }
    if (this.dataService.limit !== 50) {
      this.dataService.limit$.next(50);
    }
    if (this.dataService.blockOrderBy !== 'ts') {
      this.dataService.blockOrderBy$.next('ts');
    }
    if (this.dataService.blockOrderByAsc) {
      this.dataService.blockOrderByAsc$.next(false);
    }
    if (segment === 'blocks') {
      if (this.ready$) { this.ready$.unsubscribe(); }
      this.ready$ = this.isReady$.subscribe(finished => {
        if (finished) {
          this.loadBlocks();
        }
      });
    } else if (segment === 'payments') {
      if (this.ready$) { this.ready$.unsubscribe(); }
      this.ready$ = this.isReady$.subscribe(finished => {
        if (finished) {
          this.loadPayments();
        }
      });
    } else if (segment === 'dashboard') {

    } else if (segment === 'ports') {
      if (this.ready$) { this.ready$.unsubscribe(); }
      this.ready$ = this.isReady$.subscribe(finished => {
        if (finished) {
          this.loadPorts();
        }
      });
    }
  }

  segmentChanged(event) {
    this.handleSegment(event.detail.value);
    if (this.route$) { this.route$.unsubscribe(); }
    this.route$ = this.routing.route.params.subscribe(params => {
      this.routing.location.go('/pool/' + params.coin + '/' + encodeURIComponent(params.pool).toLowerCase() + '/' + event.detail.value);
    });
  }

  onResize(event) {
    this.innerWidth = event.target.innerWidth;
  }

  ngOnDestroy() {
    if (this.blocks$) { this.blocks$.unsubscribe(); }
    if (this.coin$) { this.coin$.unsubscribe(); }
    if (this.init$) { this.init$.unsubscribe(); }
    if (this.payments$) { this.payments$.unsubscribe(); }
    if (this.pool$) { this.pool$.unsubscribe(); }
    if (this.ports$) { this.ports$.unsubscribe(); }
    if (this.ready$) { this.ready$.unsubscribe(); }
    if (this.route$) { this.route$.unsubscribe(); }
  }

}
