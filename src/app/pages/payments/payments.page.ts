import { Component, OnInit, OnDestroy } from '@angular/core';

import { ComponentsService } from '../../services/components.service';
import { DataService } from '../../services/data.service';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
  providers: [RoutingService]
})
export class PaymentsPage implements OnInit, OnDestroy {

  constructor(
    private components: ComponentsService,
    private dataService: DataService,
    public routing: RoutingService
  ) { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.routing.location.go('/payments/' + event.detail.value);
  }

  ngOnDestroy() {
  }

}
