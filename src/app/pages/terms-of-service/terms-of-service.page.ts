import { Component, OnInit } from '@angular/core';

import { ButterCMSService } from '../../services/butter-cms.service';

@Component({
  selector: 'app-terms-of-service',
  templateUrl: './terms-of-service.page.html',
  styleUrls: ['./terms-of-service.page.scss'],
})
export class TermsOfServicePage implements OnInit {
  lastModified;
  pageContent;

  constructor(private butterCMS: ButterCMSService) { }

  async ngOnInit() {
    const data = await this.butterCMS.getPage('generic', 'terms-of-service');
    this.pageContent = data.page_content;
    this.lastModified = data.last_edited;
  }

}
