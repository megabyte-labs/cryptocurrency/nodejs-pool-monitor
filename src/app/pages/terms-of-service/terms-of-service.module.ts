import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../../shared.module';
import { TermsOfServicePage } from './terms-of-service.page';

const routes: Routes = [
  {
    path: '',
    component: TermsOfServicePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [TermsOfServicePage]
})
export class TermsOfServicePageModule {}
