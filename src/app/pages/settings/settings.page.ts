import { Component, OnInit } from '@angular/core';

import { ComponentsService } from '../../services/components.service';
import { DataService } from '../../services/data.service';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
  providers: [RoutingService]
})
export class SettingsPage implements OnInit {

  constructor(
    private components: ComponentsService,
    private dataService: DataService,
    public routing: RoutingService
  ) { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.routing.location.go('/settings/' + event.detail.value);
  }

}
