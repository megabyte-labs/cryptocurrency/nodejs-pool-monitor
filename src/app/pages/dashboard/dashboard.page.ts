import { Component, OnInit } from '@angular/core';

import { ComponentsService } from '../../services/components.service';
import { DataService } from '../../services/data.service';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
  providers: [RoutingService]
})
export class DashboardPage implements OnInit {

  constructor(
    private components: ComponentsService,
    private dataService: DataService,
    public routing: RoutingService
  ) { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.routing.location.go('/dashboard/' + event.detail.value);
  }

}
