import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'coming-soon', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'coming-soon', loadChildren: './pages/coming-soon/coming-soon.module#ComingSoonPageModule' },
  { path: 'privacy-policy', loadChildren: './pages/privacy-policy/privacy-policy.module#PrivacyPolicyPageModule' },
  { path: 'terms-of-service', loadChildren: './pages/terms-of-service/terms-of-service.module#TermsOfServicePageModule' },
  {
    path: 'help/:segment', loadChildren: './pages/help-guide/help-guide.module#HelpGuidePageModule', data: {
      type: 'help'
    }
  },
  { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
  { path: 'blog', loadChildren: './pages/blog/blog.module#BlogPageModule' },
  {
    path: 'chat/:segment', loadChildren: './pages/chat/chat.module#ChatPageModule', data: {
      type: 'chat'
    }
  },
  {
    path: 'dashboard/:segment', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule', data: {
      type: 'dashboard'
    }
  },
  {
    path: 'payments/:segment', loadChildren: './pages/payments/payments.module#PaymentsPageModule', data: {
      type: 'payment'
    }
  },
  {
    path: 'pool/:coin/:pool/:segment', loadChildren: './pages/pool/pool.module#PoolPageModule', data: {
      type: 'pool'
    }
  },
  {
    path: 'settings/:segment', loadChildren: './pages/settings/settings.module#SettingsPageModule', data: {
      type: 'setting'
    }
  },
  {
    path: 'blocks/:segment', loadChildren: './pages/blocks/blocks.module#BlocksPageModule', data: {
      type: 'block'
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
