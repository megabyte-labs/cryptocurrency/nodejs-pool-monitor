import { Component, DoCheck } from '@angular/core';
import { Platform } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { DataService } from './services/data.service';
import { RoutingService } from './services/routing.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements DoCheck {
  modalOpen = false;
  modalFadeOut = false;

  constructor(
    public dataService: DataService,
    private platform: Platform,
    public routing: RoutingService,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    console.log(this.routing.router.url);
  }

  initializeApp() {
    this.dataService.initAppDefaultSubs();
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });
  }

  closeModal() {
    this.modalFadeOut = true;
    setTimeout(() => {
      this.modalOpen = false;
      this.modalFadeOut = false;
    }, 333);
  }

  shouldShowPane() {
    if (this.routing.router.url === '/coming-soon' || window.innerWidth < 800) {
      return false;
    } else {
      return true;
    }
  }

  pageActive(page: string) {
    if (this.routing.router.url.indexOf(page) === -1) {
      return false;
    } else {
      return true;
    }
  }

  coinPageActive(pool: string) {
    const encodedPool = encodeURIComponent(pool).toLowerCase();
    if (this.routing.router.url.indexOf(encodedPool) === -1) {
      return false;
    } else {
      return true;
    }
  }

  ngDoCheck() {
  }
}
