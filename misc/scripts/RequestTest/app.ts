import * as rp from 'request-promise';

const options = {
    method: 'GET',
    uri: 'https://xmr.megabyte.space/api' + '/config'
};

async function run() {
    const response = await rp(options);
    console.log(response);
}

run()